package com.ko.hb.dto;

import java.sql.Timestamp;
import java.util.Date;

public class XfOlDpTxnLogCurrentDto {
	
	private String requestId;
	
    private String channelId;

    private String serviceCode;

    private String serviceRequestId;

    private Date dateProcess;
    
    private Date timestamp = new Date();

    private Long timeOutPeriod;

    private String requestJson;

    private String responseJson;

    private Timestamp requestTime;

    private Timestamp responseTime;

    private Long streamId;

    private String fromAccountId;

    private Long fromAccountType;

    private String fromAccountCurrency;

    private String toAccountId;

    private Long toAccountType;

    private String toAccountCurrency;

    private Long fromTransactionAmount;

    private Long fromTransactionRate;

    private Long fromTransactionAmountLce;

    private String fromMemoDescription1;

    private Long toTransactionAmount;

    private Long toTransactionRate;

    private Long toTransactionAmountLce;

    private String toMemoDescription1;

    private String bdiXFerReferenceNumber;

    private Long amount;

    private Long rate;

    private Character flagApiHandoff;

    private Character flagGefuHandoff;

    private Timestamp dateApiProcess;

    private Timestamp dateGefuProcess;

    private String responseCode;

    private String responseMessage;

    private String flagStatus;

    private String jvmIdIl;

    private String jvmIdPoller;

    private String nodeIdPoller;
    
    private String acknowledgementNo;

    private String comments;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceRequestId() {
		return serviceRequestId;
	}

	public void setServiceRequestId(String serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}

	public Date getDateProcess() {
		return dateProcess;
	}

	public void setDateProcess(Date dateProcess) {
		this.dateProcess = dateProcess;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Long getTimeOutPeriod() {
		return timeOutPeriod;
	}

	public void setTimeOutPeriod(Long timeOutPeriod) {
		this.timeOutPeriod = timeOutPeriod;
	}

	public String getRequestJson() {
		return requestJson;
	}

	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}

	public String getResponseJson() {
		return responseJson;
	}

	public void setResponseJson(String responseJson) {
		this.responseJson = responseJson;
	}

	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public Long getStreamId() {
		return streamId;
	}

	public void setStreamId(Long streamId) {
		this.streamId = streamId;
	}

	public String getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public Long getFromAccountType() {
		return fromAccountType;
	}

	public void setFromAccountType(Long fromAccountType) {
		this.fromAccountType = fromAccountType;
	}

	public String getFromAccountCurrency() {
		return fromAccountCurrency;
	}

	public void setFromAccountCurrency(String fromAccountCurrency) {
		this.fromAccountCurrency = fromAccountCurrency;
	}

	public String getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}

	public Long getToAccountType() {
		return toAccountType;
	}

	public void setToAccountType(Long toAccountType) {
		this.toAccountType = toAccountType;
	}

	public String getToAccountCurrency() {
		return toAccountCurrency;
	}

	public void setToAccountCurrency(String toAccountCurrency) {
		this.toAccountCurrency = toAccountCurrency;
	}

	public Long getFromTransactionAmount() {
		return fromTransactionAmount;
	}

	public void setFromTransactionAmount(Long fromTransactionAmount) {
		this.fromTransactionAmount = fromTransactionAmount;
	}

	public Long getFromTransactionRate() {
		return fromTransactionRate;
	}

	public void setFromTransactionRate(Long fromTransactionRate) {
		this.fromTransactionRate = fromTransactionRate;
	}

	public Long getFromTransactionAmountLce() {
		return fromTransactionAmountLce;
	}

	public void setFromTransactionAmountLce(Long fromTransactionAmountLce) {
		this.fromTransactionAmountLce = fromTransactionAmountLce;
	}

	public String getFromMemoDescription1() {
		return fromMemoDescription1;
	}

	public void setFromMemoDescription1(String fromMemoDescription1) {
		this.fromMemoDescription1 = fromMemoDescription1;
	}

	public Long getToTransactionAmount() {
		return toTransactionAmount;
	}

	public void setToTransactionAmount(Long toTransactionAmount) {
		this.toTransactionAmount = toTransactionAmount;
	}

	public Long getToTransactionRate() {
		return toTransactionRate;
	}

	public void setToTransactionRate(Long toTransactionRate) {
		this.toTransactionRate = toTransactionRate;
	}

	public Long getToTransactionAmountLce() {
		return toTransactionAmountLce;
	}

	public void setToTransactionAmountLce(Long toTransactionAmountLce) {
		this.toTransactionAmountLce = toTransactionAmountLce;
	}

	public String getToMemoDescription1() {
		return toMemoDescription1;
	}

	public void setToMemoDescription1(String toMemoDescription1) {
		this.toMemoDescription1 = toMemoDescription1;
	}

	public String getBdiXFerReferenceNumber() {
		return bdiXFerReferenceNumber;
	}

	public void setBdiXFerReferenceNumber(String bdiXFerReferenceNumber) {
		this.bdiXFerReferenceNumber = bdiXFerReferenceNumber;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getRate() {
		return rate;
	}

	public void setRate(Long rate) {
		this.rate = rate;
	}

	public Character getFlagApiHandoff() {
		return flagApiHandoff;
	}

	public void setFlagApiHandoff(Character flagApiHandoff) {
		this.flagApiHandoff = flagApiHandoff;
	}

	public Character getFlagGefuHandoff() {
		return flagGefuHandoff;
	}

	public void setFlagGefuHandoff(Character flagGefuHandoff) {
		this.flagGefuHandoff = flagGefuHandoff;
	}

	public Timestamp getDateApiProcess() {
		return dateApiProcess;
	}

	public void setDateApiProcess(Timestamp dateApiProcess) {
		this.dateApiProcess = dateApiProcess;
	}

	public Timestamp getDateGefuProcess() {
		return dateGefuProcess;
	}

	public void setDateGefuProcess(Timestamp dateGefuProcess) {
		this.dateGefuProcess = dateGefuProcess;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getFlagStatus() {
		return flagStatus;
	}

	public void setFlagStatus(String flagStatus) {
		this.flagStatus = flagStatus;
	}

	public String getJvmIdIl() {
		return jvmIdIl;
	}

	public void setJvmIdIl(String jvmIdIl) {
		this.jvmIdIl = jvmIdIl;
	}

	public String getJvmIdPoller() {
		return jvmIdPoller;
	}

	public void setJvmIdPoller(String jvmIdPoller) {
		this.jvmIdPoller = jvmIdPoller;
	}

	public String getNodeIdPoller() {
		return nodeIdPoller;
	}

	public void setNodeIdPoller(String nodeIdPoller) {
		this.nodeIdPoller = nodeIdPoller;
	}

	public String getAcknowledgementNo() {
		return acknowledgementNo;
	}

	public void setAcknowledgementNo(String acknowledgementNo) {
		this.acknowledgementNo = acknowledgementNo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
