package com.ko.hb.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.ko.hb.assembler.XfOlDpTxnLogCurrentAssembler;
import com.ko.hb.dto.XfOlDpTxnLogCurrentDto;
import com.ko.hb.entity.XfOlDpTxnLogCurrent;
import com.ko.hb.session.helper.SessionHelper;

public class XfOlDpTxnLogCurrentDao {
	
	public String createXfOlDpTxnLogCurrent(XfOlDpTxnLogCurrentDto dto) {
		XfOlDpTxnLogCurrentAssembler assembler = new XfOlDpTxnLogCurrentAssembler();
		
		SessionHelper sessionHelper = new SessionHelper();
		Session session = sessionHelper.getSession();
		
		session.beginTransaction();
		session.persist(assembler.toEntity(dto));
		session.getTransaction().commit();
		session.close();
		
		return "SUCCESS";
	}
	
	public XfOlDpTxnLogCurrentDto findById(String requestId) {
		XfOlDpTxnLogCurrentAssembler assembler = new XfOlDpTxnLogCurrentAssembler();
		
		SessionHelper sessionHelper = new SessionHelper();
		Session session = sessionHelper.getSession();
		
		XfOlDpTxnLogCurrent xfOlDpTxnLogCurrent = session.load(XfOlDpTxnLogCurrent.class, requestId);
		XfOlDpTxnLogCurrentDto xfOlDpTxnLogCurrentDto = assembler.toDto(xfOlDpTxnLogCurrent);
		return xfOlDpTxnLogCurrentDto;
	}
	
	public String updateById(String requestId, String flagStatus) {
		SessionHelper sessionHelper = new SessionHelper();
		Session session = sessionHelper.getSession();
		
		XfOlDpTxnLogCurrent xfOlDpTxnLogCurrent = session.load(XfOlDpTxnLogCurrent.class, requestId);
		xfOlDpTxnLogCurrent.setFlagStatus(flagStatus);
		
		session.beginTransaction();
		session.saveOrUpdate(xfOlDpTxnLogCurrent);
		session.getTransaction().commit();
		session.close();
		return "SUCCESS";
	}
	
	public List<XfOlDpTxnLogCurrent> getAll(){
		SessionHelper sessionHelper = new SessionHelper();
		Session session = sessionHelper.getSession();
		
		String hql = "From XfOlDpTxnLogCurrent x";
		Query query = session.createQuery(hql);
		List<XfOlDpTxnLogCurrent> list = query.list();
		return list;
	}
	

	
	public List<XfOlDpTxnLogCurrent> findByFlag(String flagStatus){
		SessionHelper sessionHelper = new SessionHelper();
		Session session = sessionHelper.getSession();
		
		String hql = "From XfOlDpTxnLogCurrent x where x.flagStatus = :flagStatus order by x.requestTime";
		Query query = session.createQuery(hql);
		query.setString("flagStatus", flagStatus);
		List<XfOlDpTxnLogCurrent> list = query.list();
		return list;
	}

}
