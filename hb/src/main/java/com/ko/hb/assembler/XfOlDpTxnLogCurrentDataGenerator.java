package com.ko.hb.assembler;

import java.sql.Timestamp;
import java.util.Date;

import com.ko.hb.dto.XfOlDpTxnLogCurrentDto;

public class XfOlDpTxnLogCurrentDataGenerator {
	
	public XfOlDpTxnLogCurrentDto getXfOlDpTxnLogCurrentDto() {
		XfOlDpTxnLogCurrentDto dto = new XfOlDpTxnLogCurrentDto();
		dto.setRequestId("REQ-00004");
        dto.setChannelId("555");
        dto.setBdiXFerReferenceNumber("8674565");
        dto.setAmount(100L);
        dto.setAcknowledgementNo("ACK90234i092i90");
        dto.setDateApiProcess(new Timestamp(System.currentTimeMillis()));
        dto.setFlagApiHandoff('Y');
        dto.setFromAccountId("123456789");
        dto.setFromAccountType(100L);
        dto.setFromAccountCurrency("USD");
        dto.setFromTransactionAmount(0L);
        dto.setFromTransactionRate(1L);
        dto.setFromTransactionAmountLce(100L);
        dto.setFromMemoDescription1("Memo 1");
        dto.setToAccountId("123456789");
        dto.setToAccountType(200L);
        dto.setToAccountCurrency("USD");
        dto.setToTransactionAmount(100L);
        dto.setToTransactionRate(1L);
        dto.setToTransactionAmountLce(100L);
        dto.setToMemoDescription1("Memo 1");
        dto.setServiceCode("SVC_CODE");
        dto.setServiceRequestId("SVC_RQ_ID");
        dto.setTimestamp(new Date());
        dto.setTimeOutPeriod(100L);
        dto.setRequestJson("REQ_JSON");
        dto.setResponseJson("RES_JSON");
        dto.setRequestTime(new Timestamp(System.currentTimeMillis()));
        dto.setResponseTime(new Timestamp(System.currentTimeMillis()));
        dto.setResponseCode("RES_CODE");
        dto.setResponseMessage("RES_MSG");
        dto.setFlagStatus("N");
        dto.setJvmIdIl("JVM_ID_IL");
        dto.setJvmIdPoller("JVM_ID_POLLER");
        dto.setComments("COMMENTS");
        dto.setFlagGefuHandoff('Y');
        dto.setDateGefuProcess(new Timestamp(System.currentTimeMillis()));
        dto.setRate(1L);
        dto.setAmount(100L);
        dto.setStreamId(100L);
        return dto;
	}

}
