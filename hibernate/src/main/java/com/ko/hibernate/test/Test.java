package com.ko.hibernate.test;

import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ko.hibernate.entity.XfOlDpTxnLogCurrent;

public class Test {

	public static void main(String[] args) {
		Configuration configuration = new Configuration();
		SessionFactory sessionFactory = configuration.configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		System.out.println("Insert started...");

		XfOlDpTxnLogCurrent xfOlDpTxnLogCurrent = new XfOlDpTxnLogCurrent();
		xfOlDpTxnLogCurrent.setRequestId("REQ-00005");
        xfOlDpTxnLogCurrent.setChannelId("555");
        xfOlDpTxnLogCurrent.setBdiXFerReferenceNumber("8674565");
        xfOlDpTxnLogCurrent.setAmount(100L);
        xfOlDpTxnLogCurrent.setAcknowledgementNo("ACK90234i092i90");
        xfOlDpTxnLogCurrent.setDateApiProcess(new Timestamp(System.currentTimeMillis()));
        xfOlDpTxnLogCurrent.setFlagApiHandoff('Y');
        xfOlDpTxnLogCurrent.setFromAccountId("123456789");
        xfOlDpTxnLogCurrent.setFromAccountType(100L);
        xfOlDpTxnLogCurrent.setFromAccountCurrency("USD");
        xfOlDpTxnLogCurrent.setFromTransactionAmount(0L);
        xfOlDpTxnLogCurrent.setFromTransactionRate(1L);
        xfOlDpTxnLogCurrent.setFromTransactionAmountLce(100L);
        xfOlDpTxnLogCurrent.setFromMemoDescription1("Memo 1");
        xfOlDpTxnLogCurrent.setToAccountId("123456789");
        xfOlDpTxnLogCurrent.setToAccountType(200L);
        xfOlDpTxnLogCurrent.setToAccountCurrency("USD");
        xfOlDpTxnLogCurrent.setToTransactionAmount(100L);
        xfOlDpTxnLogCurrent.setToTransactionRate(1L);
        xfOlDpTxnLogCurrent.setToTransactionAmountLce(100L);
        xfOlDpTxnLogCurrent.setToMemoDescription1("Memo 1");
        xfOlDpTxnLogCurrent.setServiceCode("SVC_CODE");
        xfOlDpTxnLogCurrent.setServiceRequestId("SVC_RQ_ID");
        xfOlDpTxnLogCurrent.setTimestamp(new Date());
        xfOlDpTxnLogCurrent.setTimeOutPeriod(100L);
        xfOlDpTxnLogCurrent.setRequestJson("REQ_JSON");
        xfOlDpTxnLogCurrent.setResponseJson("RES_JSON");
        xfOlDpTxnLogCurrent.setRequestTime(new Timestamp(System.currentTimeMillis()));
        xfOlDpTxnLogCurrent.setResponseTime(new Timestamp(System.currentTimeMillis()));
        xfOlDpTxnLogCurrent.setResponseCode("RES_CODE");
        xfOlDpTxnLogCurrent.setResponseMessage("RES_MSG");
        xfOlDpTxnLogCurrent.setFlagStatus("N");
        xfOlDpTxnLogCurrent.setJvmIdIl("JVM_ID_IL");
        xfOlDpTxnLogCurrent.setJvmIdPoller("JVM_ID_POLLER");
        xfOlDpTxnLogCurrent.setComments("COMMENTS");
        xfOlDpTxnLogCurrent.setFlagGefuHandoff('Y');
        xfOlDpTxnLogCurrent.setDateGefuProcess(new Timestamp(System.currentTimeMillis()));
        xfOlDpTxnLogCurrent.setRate(1L);
        xfOlDpTxnLogCurrent.setAmount(100L);
        xfOlDpTxnLogCurrent.setStreamId(100L);

        session.beginTransaction();
        session.persist(xfOlDpTxnLogCurrent);
        session.getTransaction().commit();
        session.close();
        
        System.out.println("Insert End...");
	}

}
