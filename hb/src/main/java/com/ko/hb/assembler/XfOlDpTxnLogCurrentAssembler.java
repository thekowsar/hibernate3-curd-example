package com.ko.hb.assembler;

import com.ko.hb.dto.XfOlDpTxnLogCurrentDto;
import com.ko.hb.entity.XfOlDpTxnLogCurrent;

public class XfOlDpTxnLogCurrentAssembler {
	
	public XfOlDpTxnLogCurrent toEntity(XfOlDpTxnLogCurrentDto dto) {
		
		XfOlDpTxnLogCurrent xfOlDpTxnLogCurrent = new XfOlDpTxnLogCurrent();
		xfOlDpTxnLogCurrent.setRequestId(dto.getRequestId());
        xfOlDpTxnLogCurrent.setChannelId(dto.getChannelId());
        xfOlDpTxnLogCurrent.setBdiXFerReferenceNumber(dto.getBdiXFerReferenceNumber());
        xfOlDpTxnLogCurrent.setAmount(dto.getAmount());
        xfOlDpTxnLogCurrent.setAcknowledgementNo(dto.getAcknowledgementNo());
        xfOlDpTxnLogCurrent.setDateApiProcess(dto.getDateApiProcess());
        xfOlDpTxnLogCurrent.setFlagApiHandoff(dto.getFlagApiHandoff());
        xfOlDpTxnLogCurrent.setFromAccountId(dto.getFromAccountId());
        xfOlDpTxnLogCurrent.setFromAccountType(dto.getFromAccountType());
        xfOlDpTxnLogCurrent.setFromAccountCurrency(dto.getFromAccountCurrency());
        xfOlDpTxnLogCurrent.setFromTransactionAmount(dto.getFromTransactionAmount());
        xfOlDpTxnLogCurrent.setFromTransactionRate(dto.getFromTransactionRate());
        xfOlDpTxnLogCurrent.setFromTransactionAmountLce(100L);
        xfOlDpTxnLogCurrent.setFromMemoDescription1(dto.getFromMemoDescription1());
        xfOlDpTxnLogCurrent.setToAccountId(dto.getToAccountId());
        xfOlDpTxnLogCurrent.setToAccountType(dto.getToAccountType());
        xfOlDpTxnLogCurrent.setToAccountCurrency(dto.getToAccountCurrency());
        xfOlDpTxnLogCurrent.setToTransactionAmount(dto.getToTransactionAmount());
        xfOlDpTxnLogCurrent.setToTransactionRate(dto.getToTransactionRate());
        xfOlDpTxnLogCurrent.setToTransactionAmountLce(dto.getToTransactionAmountLce());
        xfOlDpTxnLogCurrent.setToMemoDescription1(dto.getToMemoDescription1());
        xfOlDpTxnLogCurrent.setServiceCode(dto.getServiceCode());
        xfOlDpTxnLogCurrent.setServiceRequestId(dto.getServiceRequestId());
        xfOlDpTxnLogCurrent.setTimestamp(dto.getTimestamp());
        xfOlDpTxnLogCurrent.setTimeOutPeriod(dto.getTimeOutPeriod());
        xfOlDpTxnLogCurrent.setRequestJson(dto.getRequestJson());
        xfOlDpTxnLogCurrent.setResponseJson(dto.getResponseJson());
        xfOlDpTxnLogCurrent.setRequestTime(dto.getRequestTime());
        xfOlDpTxnLogCurrent.setResponseTime(dto.getRequestTime());
        xfOlDpTxnLogCurrent.setResponseCode(dto.getResponseCode());
        xfOlDpTxnLogCurrent.setResponseMessage(dto.getResponseMessage());
        xfOlDpTxnLogCurrent.setFlagStatus(dto.getFlagStatus());
        xfOlDpTxnLogCurrent.setJvmIdIl(dto.getJvmIdIl());
        xfOlDpTxnLogCurrent.setJvmIdPoller(dto.getJvmIdPoller());
        xfOlDpTxnLogCurrent.setNodeIdPoller(dto.getNodeIdPoller());
        xfOlDpTxnLogCurrent.setComments(dto.getComments());
        xfOlDpTxnLogCurrent.setFlagGefuHandoff(dto.getFlagApiHandoff());
        xfOlDpTxnLogCurrent.setDateGefuProcess(dto.getDateGefuProcess());
        xfOlDpTxnLogCurrent.setRate(dto.getRate());
        xfOlDpTxnLogCurrent.setAmount(dto.getAmount());
        xfOlDpTxnLogCurrent.setStreamId(dto.getStreamId());
        
        return xfOlDpTxnLogCurrent;
	}
	
	public XfOlDpTxnLogCurrentDto toDto(XfOlDpTxnLogCurrent entity) {
		XfOlDpTxnLogCurrentDto dto = new XfOlDpTxnLogCurrentDto();
		dto.setRequestId(entity.getRequestId());
        dto.setChannelId(entity.getChannelId());
        dto.setBdiXFerReferenceNumber(entity.getBdiXFerReferenceNumber());
        dto.setAmount(entity.getAmount());
        dto.setAcknowledgementNo(entity.getAcknowledgementNo());
        dto.setDateApiProcess(entity.getDateApiProcess());
        dto.setFlagApiHandoff(entity.getFlagApiHandoff());
        dto.setFromAccountId(entity.getFromAccountId());
        dto.setFromAccountType(entity.getFromAccountType());
        dto.setFromAccountCurrency(entity.getFromAccountCurrency());
        dto.setFromTransactionAmount(entity.getFromTransactionAmount());
        dto.setFromTransactionRate(entity.getFromTransactionRate());
        dto.setFromTransactionAmountLce(100L);
        dto.setFromMemoDescription1(entity.getFromMemoDescription1());
        dto.setToAccountId(entity.getToAccountId());
        dto.setToAccountType(entity.getToAccountType());
        dto.setToAccountCurrency(entity.getToAccountCurrency());
        dto.setToTransactionAmount(entity.getToTransactionAmount());
        dto.setToTransactionRate(entity.getToTransactionRate());
        dto.setToTransactionAmountLce(entity.getToTransactionAmountLce());
        dto.setToMemoDescription1(entity.getToMemoDescription1());
        dto.setServiceCode(entity.getServiceCode());
        dto.setServiceRequestId(entity.getServiceRequestId());
        dto.setTimestamp(entity.getTimestamp());
        dto.setTimeOutPeriod(entity.getTimeOutPeriod());
        dto.setRequestJson(entity.getRequestJson());
        dto.setResponseJson(entity.getResponseJson());
        dto.setRequestTime(entity.getRequestTime());
        dto.setResponseTime(entity.getRequestTime());
        dto.setResponseCode(entity.getResponseCode());
        dto.setResponseMessage(entity.getResponseMessage());
        dto.setFlagStatus(entity.getFlagStatus());
        dto.setJvmIdIl(entity.getJvmIdIl());
        dto.setJvmIdPoller(entity.getJvmIdPoller());
        dto.setNodeIdPoller(entity.getNodeIdPoller());
        dto.setComments(entity.getComments());
        dto.setFlagGefuHandoff(entity.getFlagApiHandoff());
        dto.setDateGefuProcess(entity.getDateGefuProcess());
        dto.setRate(entity.getRate());
        dto.setAmount(entity.getAmount());
        dto.setStreamId(entity.getStreamId());
        
        return dto;
	}

}
