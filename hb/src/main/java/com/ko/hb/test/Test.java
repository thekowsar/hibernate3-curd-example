package com.ko.hb.test;

import java.util.List;

import com.ko.hb.assembler.XfOlDpTxnLogCurrentDataGenerator;
import com.ko.hb.dao.XfOlDpTxnLogCurrentDao;
import com.ko.hb.dto.XfOlDpTxnLogCurrentDto;
import com.ko.hb.entity.XfOlDpTxnLogCurrent;

public class Test {

	public static void main(String[] args) {
		System.out.println("Start creating ....");
		
//		XfOlDpTxnLogCurrentDto dto = new XfOlDpTxnLogCurrentDataGenerator().getXfOlDpTxnLogCurrentDto();
//        String message = new XfOlDpTxnLogCurrentDao().createXfOlDpTxnLogCurrent(dto);
//        System.out.println("Result : " + message);
		
//		XfOlDpTxnLogCurrentDto dto = new XfOlDpTxnLogCurrentDao().findById("REQ-00004");
//		System.out.println("Request Id : " + dto.getRequestId());
//		System.out.println("Channer Id : " + dto.getChannelId());
//		System.out.println("Service Code : " + dto.getServiceCode());
//		System.out.println("Service Request Id : " + dto.getServiceRequestId());
		
//		String message = new XfOlDpTxnLogCurrentDao().updateById("REQ-00004", "U");
//		System.out.println("Result : " + message);
		
//		List<XfOlDpTxnLogCurrent> list = new XfOlDpTxnLogCurrentDao().getAll();
//		for(XfOlDpTxnLogCurrent entity : list) {
//			System.out.println("Request Id : " + entity.getRequestId());
//			System.out.println("Channer Id : " + entity.getChannelId());
//			System.out.println("Service Code : " + entity.getServiceCode());
//			System.out.println("Service Request Id : " + entity.getServiceRequestId());
//			System.out.println("----------------------------------");
//		}
		
		List<XfOlDpTxnLogCurrent> list = new XfOlDpTxnLogCurrentDao().findByFlag("N");
		for(XfOlDpTxnLogCurrent entity : list) {
			System.out.println("Request Id : " + entity.getRequestId());
			System.out.println("Channer Id : " + entity.getChannelId());
			System.out.println("Service Code : " + entity.getServiceCode());
			System.out.println("Service Request Id : " + entity.getServiceRequestId());
			System.out.println("Flag : " + entity.getFlagStatus());
			System.out.println("----------------------------------");
		}
        
        System.out.println("End creating ....");
	}

}
